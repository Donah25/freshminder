package task.freshminder.freshminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.List;


public class ExpiredActivity extends Fragment {

    RecyclerView recyclerView;
    MyExpiredAdapter myRecyclerViewAdapter;
    DatabaseHandler databaseHandler;
    List<itemModel> data = new ArrayList<>();
    int numdays;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_expired, container, false);
        //getSupportActionBar().setLogo(R.drawable.bars);

        setHasOptionsMenu(true);

        databaseHandler = new DatabaseHandler(getActivity());
        final List<itemModel> pData = databaseHandler.getAllData();


        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewExpired);



        for (itemModel cn : pData) {

            try {

                //yyyy-mm-dd
                String datee = cn.getExpire_date();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date todayDate = new Date();
                String thisDate = df.format(todayDate);

                String parts1[] = thisDate.split("-");

                int day1 = Integer.parseInt(parts1[2]);
                int month1 = Integer.parseInt(parts1[1]);
                int year1 = Integer.parseInt(parts1[0]);

                int resultMonth = month1 - 1;

                String currentDate = year1 + "-" + resultMonth + "-" + day1;

                Date set_date = null;
                Date current_date = null;

                set_date = df.parse(datee);
                current_date = df.parse(currentDate);

                //86400000
                long diff = set_date.getTime() - current_date.getTime();

                long days = diff / (24 * 60 * 60 * 1000);

                numdays = (int) days;

                Log.d("ExDate: ", cn.getExpire_date());
                Log.d("dateNow: ", Long.toString(days));
                Log.d("Date: ", currentDate);

            } catch (ParseException e) {
                Log.e("TEST", "Exception", e);
            }

            itemModel itemData = new itemModel();


            if (numdays <= 0) {

                itemData.item_name = cn.getItem_name();
                itemData.random_num = cn.getRandom_num();
                itemData.expire_date = cn.getExpire_date();
                itemData.remind_me = cn.getRemind_me();
                itemData.num_days = Integer.toString(numdays);
                data.add(itemData);
            }

        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myRecyclerViewAdapter = new MyExpiredAdapter(getActivity(), data);
        recyclerView.setAdapter(myRecyclerViewAdapter);

        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorPrimaryDark));


        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {

            @Override
            public boolean onQueryTextChange(String query) {
                final List<itemModel> filteredModelList = filter(data, query);

                myRecyclerViewAdapter.setFilter(filteredModelList);

                return true;

            }
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

        });


        super.onCreateOptionsMenu(menu, inflater);


    }
    private List<itemModel> filter(List<itemModel> models, String query) {
        query = query.toLowerCase();
        final List<itemModel> filteredModelList = new ArrayList<>();
        for (itemModel model : models) {
            final String text = model.item_name.toLowerCase();
            final String cname = model.item_name.toLowerCase();
            if (text.contains(query) || cname.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.action_search:   //this item has your app icon
                return true;
            case android.R.id.home:

                getActivity().finish();
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }



}

