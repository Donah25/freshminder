package task.freshminder.freshminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 8/20/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {




    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Freshminder";

    // table name
    private static final String TABLE_LIST = "remind_list";


    // Table Columns names
    private static final String KEY_ID = "id";
    private static final String item_name = "item_name";
    private static final String expire_date = "expire_date";
    private static final String remind = "remind";
    private static final String note = "note";
    private static final String position = "position";
    private static final String random_id = "random_id";
    private static final String timeSet = "time_set";


    private static final String CREATE_LIST_TABLE = "CREATE TABLE " + TABLE_LIST + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + item_name + " TEXT,"
            + expire_date + " TEXT," + remind + " TEXT," + note + " TEXT," + position + " TEXT," + random_id + " TEXT," + timeSet + " TEXT"+")";




    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_LIST_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    void addData(itemModel itemModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(item_name, itemModel.get_id());
        values.put(item_name, itemModel.getItem_name());
        values.put(expire_date, itemModel.getExpire_date());
        values.put(remind, itemModel.getRemind_me());
        values.put(note, itemModel.getNotes());
        values.put(position, itemModel.getPosition());
        values.put(random_id, itemModel.getRandom_num());
        values.put(timeSet, itemModel.getTimeSet());
        // Inserting Row
        db.insert(TABLE_LIST, null, values);
        db.close(); // Closing database connection
    }


    // Getting All Contacts
    public List<itemModel> getAllData() {
        List<itemModel> pList = new ArrayList<itemModel>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_LIST + " ORDER BY " ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_LIST, null, null, null, null, null, expire_date + " ASC", null);
       // Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                itemModel pojodata = new itemModel();
                pojodata.set_id(Integer.parseInt(cursor.getString(0)));
                pojodata.setItem_name(cursor.getString(1));
                pojodata.setExpire_date(cursor.getString(2));
                pojodata.setRemind_me(cursor.getString(3));
                pojodata.setNotes(cursor.getString(4));
                pojodata.setPosition(cursor.getString(5));
                pojodata.setRandom_num(cursor.getString(6));
                pojodata.setTimeSet(cursor.getString(7));

                pList.add(pojodata);
            } while (cursor.moveToNext());
        }

        // return contact list
        return pList;
    }



    public Integer removeItem(String random_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return  db.delete(TABLE_LIST, "random_id = ?",
                new String[] {random_id} );
    }


    public int updateData(String itemname, String expiredate, String note_, String remindme, String pos, String randomid, String time_set) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(item_name, itemname);
        values.put(expire_date, expiredate);
        values.put(remind, remindme);
        values.put(note, note_);
        values.put(position, pos);
        values.put(timeSet, time_set);

        // updating row
        return db.update(TABLE_LIST, values,"random_id = ?",
                new String[] {randomid});
    }

}