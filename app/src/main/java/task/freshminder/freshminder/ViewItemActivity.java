package task.freshminder.freshminder;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by acer on 8/20/2017.
 */

public class ViewItemActivity extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener {

    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";

    TextView deletetv, savetv, setDateView, setDateView1, setTime;
    EditText item_et, note_et;
    CalendarView calendarV;
    Spinner spinnerView;
    DatabaseHandler databaseHandler;
    String itemV, posV, dateV, timeV;
    int day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewitem);

        final Intent intent = ViewItemActivity.this.getIntent();
        databaseHandler = new DatabaseHandler(ViewItemActivity.this);
        final List<itemModel> pData = databaseHandler.getAllData();

        deletetv = (TextView) findViewById(R.id.delete);
        savetv = (TextView) findViewById(R.id.saveView);
        item_et = (EditText) findViewById(R.id.item_nameView);
        note_et = (EditText) findViewById(R.id.noteView);
        spinnerView = (Spinner) findViewById(R.id.spinnerRemindView);
        setDateView = (TextView) findViewById(R.id.setDateView);
        setDateView1 = (TextView) findViewById(R.id.setDateView1);
        setTime = (TextView) findViewById(R.id.set_timeView);

       //Toast.makeText(ViewItemActivity.this, intent.getStringExtra("random_id"), Toast.LENGTH_SHORT).show();

        for (itemModel cn : pData) {
            if (cn.getRandom_num().equals(intent.getStringExtra("random_id"))){

                item_et.setText(cn.getItem_name());
                note_et.setText(cn.getNotes());
                spinnerView.setSelection(Integer.parseInt(cn.getPosition()));
                setDateView1.setText(cn.getExpire_date());
                setTime.setText(cn.getTimeSet());


                dateV = cn.getExpire_date(); //yyyy-mm-dd
                String parts[] = dateV.split("-");

                 day = Integer.parseInt(parts[2]);
                 month = Integer.parseInt(parts[1]);
                 year = Integer.parseInt(parts[0]);

                String MONTH = "";

                if (month == 0){
                    MONTH = "January";
                }else if(month == 1){
                    MONTH = "February";
                }else if(month == 2){
                    MONTH = "March";
                }else if(month == 3){
                    MONTH = "April";
                }else if(month == 4){
                    MONTH = "May";
                }else if(month == 5){
                    MONTH = "June";
                }else if(month == 6){
                    MONTH = "July";
                }else if(month == 7){
                    MONTH = "August";
                }else if(month == 8){
                    MONTH = "September";
                }else if(month == 9){
                    MONTH = "October";
                }else if(month == 10){
                    MONTH = "November";
                }else if(month == 11){
                    MONTH = "December";
                }

                setDateView.setText(MONTH + " " + day + ", " + year);


               // Toast.makeText(ViewItemActivity.this, Integer.toString(year) + Integer.toString(month) + Integer.toString(day), Toast.LENGTH_SHORT).show();
                /*Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                long milliTime = calendar.getTimeInMillis();

                calendarV.setDate (milliTime, true, true);*/

                // calendarV.setDate(Long.parseLong(cn.getExpire_date()));*/


            }
        }

          /*  calendarV.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {

                    dateV = i + "-" + i1 + "-" + i2; //yyyy-mm-dd

                }
            });
*/
        spinnerView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                itemV = parent.getItemAtPosition(position).toString();
                posV =  Integer.toString(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        SimpleDateFormat min = new SimpleDateFormat("mm");
        final String currentHour = sdf.format(c.getTime());
        final String currentMin = min.format(c.getTime());

        SimpleDateFormat format = new SimpleDateFormat("HH");
        //String hour = format.format(new Date());

        Calendar calendar = Calendar.getInstance();
        final int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);

        String AM_PM ;
        if(hourOfDay < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }



        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(ViewItemActivity.this)
                        .setStartTime(hourOfDay, Integer.parseInt(currentMin))
                        .setDoneText("Done")
                        .setCancelText("")
                        .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
                rtpd.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);
            }
        });
        setDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(ViewItemActivity.this)
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setDoneText("Done")
                        .setCancelText("")
                        .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
                cdp.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
                //here
               cdp.setPreselectedDate(year, month, day);
            }
        });


        savetv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String item_name = item_et.getText().toString();
                String note = note_et.getText().toString();
                dateV = setDateView1.getText().toString();
                timeV = setTime.getText().toString();

                //String item_name, String expire_date, String notes, String remind_me, String position, String random_id
                Integer updateRows =  databaseHandler.updateData(item_name, dateV, note, itemV, posV, intent.getStringExtra("random_id"), timeV);

                if (updateRows>0){

                    Intent intent = new Intent(ViewItemActivity.this, NavDrawerActivity.class);
                    startActivity(intent);
                    finish();
                    Toast.makeText(ViewItemActivity.this, "Item updated!", Toast.LENGTH_SHORT).show();


                }else {
                    Toast.makeText(ViewItemActivity.this, "Item was not updated!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        deletetv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer deleteRows = databaseHandler.removeItem(intent.getStringExtra("random_id"));


                if (deleteRows>0){

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ViewItemActivity.this);

                    // Setting Dialog Title
                    alertDialog.setTitle("Alert!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want delete this?");


                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {

                            Toast.makeText(ViewItemActivity.this, "Item Removed!", Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(ViewItemActivity.this, NavDrawerActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();



                }else {
                    Toast.makeText(ViewItemActivity.this, "Item was not Removed!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        String MONTH = "";

        if (monthOfYear == 0){
            MONTH = "January";
        }else if(monthOfYear == 1){
            MONTH = "February";
        }else if(monthOfYear == 2){
            MONTH = "March";
        }else if(monthOfYear == 3){
            MONTH = "April";
        }else if(monthOfYear == 4){
            MONTH = "May";
        }else if(monthOfYear == 5){
            MONTH = "June";
        }else if(monthOfYear == 6){
            MONTH = "July";
        }else if(monthOfYear == 7){
            MONTH = "August";
        }else if(monthOfYear == 8){
            MONTH = "September";
        }else if(monthOfYear == 9){
            MONTH = "October";
        }else if(monthOfYear == 10){
            MONTH = "November";
        }else if(monthOfYear == 11){
            MONTH = "December";
        }

        setDateView.setText(MONTH + " " + dayOfMonth + ", " + year);
        setDateView1.setText(getString(R.string.calendar_date_picker_result_values, year, monthOfYear, dayOfMonth));
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        String AM_PM ;
        if(hourOfDay < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        if (hourOfDay == 0){
            setTime.setText(12 + " : " + minute + " " + AM_PM);
        }else{
            setTime.setText(hourOfDay + " : " + minute + " " + AM_PM);
        }


    }

    @Override
    public void onResume() {
        // Example of reattaching to the fragment
        super.onResume();
        CalendarDatePickerDialogFragment calendarDatePickerDialogFragment = (CalendarDatePickerDialogFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAG_TAG_DATE_PICKER);
        if (calendarDatePickerDialogFragment != null) {
            calendarDatePickerDialogFragment.setOnDateSetListener(this);
        }
    }
    @Override
    public void onBackPressed() {

        moveTaskToBack(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
