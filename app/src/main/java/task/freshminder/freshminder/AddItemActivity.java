package task.freshminder.freshminder;


import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;


/**
 * Created by acer on 8/20/2017.
 */

public class AddItemActivity extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener {

    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";

    DatabaseHandler databaseHandler;
    EditText etItem, etNote;
    Spinner spinnerRemind;
    TextView save, cancel;
    itemModel itemModel;
    String item, date, pos, time;
    TextView setDate, setTime, setDate1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additem);

        databaseHandler = new DatabaseHandler(AddItemActivity.this);

        etItem = (EditText)findViewById(R.id.item_name);
        etNote = (EditText)findViewById(R.id.note);
        spinnerRemind = (Spinner) findViewById(R.id.spinnerRemind);
        save = (TextView) findViewById(R.id.save);
        setDate = (TextView) findViewById(R.id.setDate);
        setDate1 = (TextView) findViewById(R.id.setDate1);
        setTime = (TextView) findViewById(R.id.set_time);
        cancel = (TextView) findViewById(R.id.cancel);


      /*  calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {
                //dateDisplay.setText("Date: " + i2 + " / " + i1 + " / " + i);
                date = i + "-" + i1 + "-" + i2; //yyyy-mm-dd
               // Toast.makeText(getApplicationContext(), "Selected Date:\n" + "Day = " + i2 + "\n" + "Month = " + i1 + "\n" + "Year = " + i, Toast.LENGTH_LONG).show();
            }
        });*/
         spinnerRemind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                             item = parent.getItemAtPosition(position).toString();
                             pos =  Integer.toString(position);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
        spinnerRemind.setSelection(1);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddItemActivity.this, NavDrawerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item_name = etItem.getText().toString();
                String note = etNote.getText().toString();
                date = setDate1.getText().toString();
                time = setTime.getText().toString();

                if (date.equals("(Tap to set date)")) {
                    Toast.makeText(AddItemActivity.this, "Please select date", Toast.LENGTH_SHORT).show();
                }

                if (TextUtils.isEmpty(item_name)){
                    etItem.setError("Please input item name");
                }

                if (!date.equals("(Tap to set date)") && !item_name.isEmpty()) {
                    Random r = new Random();
                    int i1 = r.nextInt(100 - 10) + 10;
                    Log.d("Random:", Integer.toString(i1));

                    //Toast.makeText(AddItemActivity.this, pos, Toast.LENGTH_SHORT).show();

                    databaseHandler.addData(new itemModel(item_name, date, note, item, pos, Integer.toString(i1), time));

                    Toast.makeText(AddItemActivity.this, "Item saved!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AddItemActivity.this, NavDrawerActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please input data.", Toast.LENGTH_SHORT)
                            .show();

                }


            }
        });


        setDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(AddItemActivity.this)
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setDoneText("Done")
                        .setCancelText("")
                        .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
                cdp.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        SimpleDateFormat min = new SimpleDateFormat("mm");
        final String currentHour = sdf.format(c.getTime());
        final String currentMin = min.format(c.getTime());

        SimpleDateFormat format = new SimpleDateFormat("HH");
        //String hour = format.format(new Date());

        Calendar calendar = Calendar.getInstance();
        final int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);

        String AM_PM ;
        if(hourOfDay < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }

        setTime.setText(hourOfDay + " : " + currentMin + " " + AM_PM);

        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(AddItemActivity.this)
                        .setStartTime(hourOfDay, Integer.parseInt(currentMin))
                        .setDoneText("Done")
                        .setCancelText("")
                        .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
                rtpd.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);
            }
        });



    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        String MONTH = "";

        if (monthOfYear == 0){
            MONTH = "January";
        }else if(monthOfYear == 1){
            MONTH = "February";
        }else if(monthOfYear == 2){
            MONTH = "March";
        }else if(monthOfYear == 3){
            MONTH = "April";
        }else if(monthOfYear == 4){
            MONTH = "May";
        }else if(monthOfYear == 5){
            MONTH = "June";
        }else if(monthOfYear == 6){
            MONTH = "July";
        }else if(monthOfYear == 7){
            MONTH = "August";
        }else if(monthOfYear == 8){
            MONTH = "September";
        }else if(monthOfYear == 9){
            MONTH = "October";
        }else if(monthOfYear == 10){
            MONTH = "November";
        }else if(monthOfYear == 11){
            MONTH = "December";
        }

        setDate.setText(MONTH + " " + dayOfMonth + ", " + year);

      setDate1.setText(getString(R.string.calendar_date_picker_result_values, year, monthOfYear, dayOfMonth));
    }
    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        String AM_PM ;
        if(hourOfDay < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        if (hourOfDay == 0){
            setTime.setText(12 + " : " + minute + " " + AM_PM);
        }else{
            setTime.setText(hourOfDay + " : " + minute + " " + AM_PM);
        }


    }


    @Override
    public void onResume() {
        // Example of reattaching to the fragment
        super.onResume();
        CalendarDatePickerDialogFragment calendarDatePickerDialogFragment = (CalendarDatePickerDialogFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAG_TAG_DATE_PICKER);
        if (calendarDatePickerDialogFragment != null) {
            calendarDatePickerDialogFragment.setOnDateSetListener(this);
        }
        RadialTimePickerDialogFragment rtpd = (RadialTimePickerDialogFragment) getSupportFragmentManager().findFragmentByTag(FRAG_TAG_TIME_PICKER);
        if (rtpd != null) {
            rtpd.setOnTimeSetListener(this);
        }
    }
    @Override
    public void onBackPressed() {

        moveTaskToBack(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
