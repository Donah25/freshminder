package task.freshminder.freshminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by acer on 9/22/2017.
 */

public class NotificationService extends Service{
    String initData;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

          mTimer = new Timer();
          mTimer.schedule(timerTask, 2000, 86400000*1000);

        return START_STICKY;
    }



        private Timer mTimer;

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("Log", "Running");
                notifyy();
            }
        };

    @Override
    public void onDestroy() {
        try {
           // mTimer.cancel();
            timerTask.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        Intent intent = new Intent("com.example.sample");
        intent.putExtra("value", "value");
        sendBroadcast(intent);
    }

  public void notifyy(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullservice");

        Intent myintent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT);
        Context context = getApplicationContext();

        Notification.Builder builder;

        builder = new Notification.Builder(context)
                .setContentTitle("Sample")
                .setContentText("Sample ghpon")
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setSmallIcon(R.drawable.freshminder);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

    }


}
