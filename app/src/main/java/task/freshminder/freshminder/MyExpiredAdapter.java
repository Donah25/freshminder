package task.freshminder.freshminder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



class MyExpiredAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private LayoutInflater inflater;
    List<itemModel> mdata = Collections.emptyList();
    DatabaseHandler databaseHandler;


    public MyExpiredAdapter(List<itemModel> rdata) {
        this.mdata = rdata;

    }


    // create constructor to innitilize context and data sent from MainActivity
    public MyExpiredAdapter(Context context, List<itemModel> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mdata = data;
    }

    // Inflate the layout when viewholder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activity_expired_list, parent, false);


        MyHolder holder = new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        databaseHandler = new DatabaseHandler(context);
        final MyHolder myHolder = (MyHolder) holder;
        final itemModel current = mdata.get(position);
        myHolder.info.setText(current.item_name);


        myHolder.restock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Integer deleteRows = databaseHandler.removeItem(current.random_num);


                if (deleteRows>0){
                    mdata.remove(position);
                    notifyItemRemoved(position);

                         Toast.makeText(context, "Item deleted!", Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(context, "Item was not deleted!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    // return total item from List
    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public List<itemModel> getPrice() {
        return mdata;
    }


    class MyHolder extends RecyclerView.ViewHolder {


        TextView info;
        Button restock;

        public MyHolder(View itemView) {
            super(itemView);
            info = (TextView) itemView.findViewById(R.id.info_textExpired);
            restock = (Button) itemView.findViewById(R.id.restock);

        }

    }

    public void setFilter(List<itemModel> countryModels) {
        mdata = new ArrayList<>();
        mdata.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void refreshItems(List<itemModel> dataLoad){
        this.mdata = dataLoad;
        notifyDataSetChanged();
    }


}
