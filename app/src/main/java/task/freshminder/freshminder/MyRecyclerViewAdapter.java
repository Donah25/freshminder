package task.freshminder.freshminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by acer on 8/19/2017.
 */

class MyRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private LayoutInflater inflater;
    List<itemModel> mdata = Collections.emptyList();
    DatabaseHandler databaseHandler;


    public MyRecyclerViewAdapter(List<itemModel> rdata) {
        this.mdata = rdata;

    }


    // create constructor to innitilize context and data sent from MainActivity
    public MyRecyclerViewAdapter(Context context, List<itemModel> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mdata = data;
    }

    // Inflate the layout when viewholder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_cards, parent, false);


        MyHolder holder = new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        databaseHandler = new DatabaseHandler(context);
        final MyHolder myHolder = (MyHolder) holder;
        final itemModel current = mdata.get(position);
        myHolder.info.setText(current.item_name);

      /*  //yyyy-mm-dd
        String datee = current.expire_date;
        String parts[] = datee.split("-");

        int day = Integer.parseInt(parts[2]);
        int month = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[0]);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = new Date();
        String thisDate = df.format(todayDate);

        String parts1[] = thisDate.split("-");

        int day1 = Integer.parseInt(parts1[2]);
        int month1 = Integer.parseInt(parts1[1]);
        int year1 = Integer.parseInt(parts1[0]);

        int resultMonth = month1 - 1;

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.YEAR, year1);
        calendar1.set(Calendar.MONTH, resultMonth);
        calendar1.set(Calendar.DAY_OF_MONTH, day1);

        //86400000
        long diff = calendar.getTimeInMillis() - calendar1.getTimeInMillis();

        long days = diff / (24 * 60 * 60 * 1000);

        int numdays = (int)days; */




//        Log.d("ExDate: ", current.expire_date);
       // Log.d("dateNow: ", Long.toString(days));
      //  Log.d("Date: ", current.num_days);

        if (Integer.parseInt(current.num_days) <=0){
            myHolder.day.setText("Expired");
        }else{
            myHolder.day.setText(current.days_format);
        }



        myHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewItemActivity.class);
                intent.putExtra("item_id", current.item_name);
                intent.putExtra("random_id", current.random_num);
                context.startActivity(intent);

            }
        });

    }

    // return total item from List
    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public List<itemModel> getPrice() {
        return mdata;
    }


    class MyHolder extends RecyclerView.ViewHolder {


        TextView info, day, empty;
        RelativeLayout relativeLayout;

        public MyHolder(View itemView) {
            super(itemView);
            info = (TextView) itemView.findViewById(R.id.info_text);
            day = (TextView) itemView.findViewById(R.id.day_text);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.cardView);

        }

    }

    public void setFilter(List<itemModel> countryModels) {
        mdata = new ArrayList<>();
        mdata.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void refreshItems(List<itemModel> dataLoad){
        this.mdata = dataLoad;
        notifyDataSetChanged();
    }


}