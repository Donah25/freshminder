package task.freshminder.freshminder;

/**
 * Created by acer on 8/19/2017.
 */

public class itemModel {
    public int _id;
    public String item_name;
    public String expire_date;
    public String notes;
    public String remind_me;
    public String position;
    public String random_num;
    public String timeSet;
    public String num_days;
    public String days_format;



    public itemModel() {

    }

    public itemModel(int _id, String item_name, String expire_date, String notes, String remind_me,String position, String random_num, String timeSet) {
        this._id = _id;
        this.item_name = item_name;
        this.expire_date = expire_date;
        this.notes = notes;
        this.remind_me = remind_me;
        this.position = position;
        this.random_num = random_num;
        this.timeSet = timeSet;
    }

    public itemModel(String item_name, String expire_date, String notes, String remind_me, String position, String random_num, String timeSet) {
        this.item_name = item_name;
        this.expire_date = expire_date;
        this.notes = notes;
        this.remind_me = remind_me;
        this.position = position;
        this.random_num = random_num;
        this.timeSet = timeSet;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRemind_me() {
        return remind_me;
    }

    public void setRemind_me(String remind_me) {
        this.remind_me = remind_me;
    }

    public String getDays_format() {
        return days_format;
    }

    public void setDays_format(String days_format) {
        this.days_format = days_format;
    }

    public String getNum_days() {
        return num_days;
    }

    public void setNum_days(String num_days) {
        this.num_days = num_days;
    }

    public String getRandom_num() {
        return random_num;
    }

    public void setRandom_num(String random_num) {
        this.random_num = random_num;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTimeSet() {
        return timeSet;
    }

    public void setTimeSet(String timeSet) {
        this.timeSet = timeSet;
    }



}
