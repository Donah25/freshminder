package task.freshminder.freshminder;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class MainActivity extends Fragment{

    RecyclerView recyclerView;
    MyRecyclerViewAdapter myRecyclerViewAdapter;
    DatabaseHandler databaseHandler;
    List<itemModel> data = new ArrayList<>();
    int numdays;
    long diffSeconds, diffMinutes,diffHours;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_main, container, false);
        //getSupportActionBar().setLogo(R.drawable.bars);
        setHasOptionsMenu(true);

        databaseHandler = new DatabaseHandler(getActivity());
        final List<itemModel> pData = databaseHandler.getAllData();


        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);


        for (itemModel cn : pData) {

            try {

                //yyyy-mm-dd
                String datee = cn.getExpire_date();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date todayDate = new Date();
                String thisDate = df.format(todayDate);

                String parts1[] = thisDate.split("-");

                int day1 = Integer.parseInt(parts1[2]);
                int month1 = Integer.parseInt(parts1[1]);
                int year1 = Integer.parseInt(parts1[0]);

                int resultMonth = month1 - 1;

                String currentDate = year1 + "-" + resultMonth + "-" + day1;

                Date set_date = null;
                Date current_date = null;

                set_date = df.parse(datee);
                current_date = df.parse(currentDate);

                //86400000
                long diff = set_date.getTime() - current_date.getTime();

                long days = diff / (24 * 60 * 60 * 1000);

                numdays = (int)days;

                Log.d("ExDate: ", cn.getExpire_date());
                Log.d("dateNow: ", Long.toString(days));
                Log.d("Date: ", currentDate);

            } catch (ParseException e) {
                Log.e("TEST", "Exception", e);
            }
            String DAY_FORMAT= numdays + "d";

           /* if (numdays == 1){
                DAY_FORMAT = numdays + " day";
            }else if(numdays <7){
                DAY_FORMAT = numdays + " days";
            }else if (numdays >=7 && numdays<30){
                int count_weeks = numdays/7;
                int mod_days = numdays%7;
                if (mod_days == 0 && count_weeks == 1){
                    DAY_FORMAT = count_weeks + " week";
                }else if(mod_days == 0 && count_weeks > 1) {
                    DAY_FORMAT = count_weeks + " weeks";
                } /*else if(mod_days > 1 && count_weeks == 1){

                    DAY_FORMAT = count_weeks + " week & " + mod_days + " days";
                }else if(mod_days == 1 && count_weeks == 1){

                    DAY_FORMAT = count_weeks + " week & " + mod_days + " day";
                }else if(mod_days > 1 && count_weeks > 1){

                    DAY_FORMAT = count_weeks + " weeks & " + mod_days + " days";
                }
                else if(mod_days == 1 && count_weeks > 1){

                    DAY_FORMAT = count_weeks + " weeks & " + mod_days + " day";
                }

            }else if (numdays >= 30 && numdays<365){
                int count_month = numdays/30;
                int mod_month = numdays%30;
                int modu_week = mod_month/7;

                if (count_month == 1){
                    DAY_FORMAT = count_month + " month";
                }else {
                    DAY_FORMAT = count_month + " months";
                }
                /*else if (mod_month <7){
                    DAY_FORMAT = count_month + " month(s) & " + mod_month + " day(s)";
                }else if (mod_month >=7  && mod_month<30){
                    DAY_FORMAT = count_month + " month(s) & " + modu_week + " week(s)";
                }
            }else if (numdays>=365){
                int count_year = numdays/365;
                int mod_month = numdays%365;
                int modu_week = mod_month/7;

                if (count_year == 1){
                    DAY_FORMAT = count_year + " year";
                }else{
                    DAY_FORMAT = count_year + " years";
                }
                /*else if (mod_month <7 & mod_month>0){
                    DAY_FORMAT = count_year + " year(s) & " + mod_month + " day(s)";
                }else if (mod_month >=7 && mod_month<30){
                    if (modu_week>=4){
                        DAY_FORMAT = count_year + " year(s) & " + modu_week + " week(s)";
                    }else{
                        DAY_FORMAT = count_year + " year(s) & " + mod_month+ " month(s)";
                    }
                }
            }*/

            String log = "Id: "+cn.getItem_name();
            itemModel itemData = new itemModel();
            Log.d("time: ", cn.getTimeSet());
          if (numdays >=1 ){

                itemData.item_name = cn.getItem_name();
                itemData.random_num = cn.getRandom_num();
                itemData.expire_date = cn.getExpire_date();
                itemData.remind_me = cn.getRemind_me();
                itemData.days_format = DAY_FORMAT;
                itemData.num_days = Integer.toString(numdays);
                data.add(itemData);
            }
            String expireTime = "";
            String noSpaceTime = "";
            String am_pm = "";
            if (cn.getTimeSet().contains("PM")){
                expireTime = cn.getTimeSet().replace("PM", "");
                noSpaceTime = expireTime.replace(" ", "") + ":00";
                am_pm = "1";
            }else if (cn.getTimeSet().contains("AM")) {
                expireTime = cn.getTimeSet().replace("AM", "");
                noSpaceTime = expireTime.replace(" ", "") + ":00";
                am_pm = "0";
            }

            Log.e("trim time", expireTime + noSpaceTime);
            String splitTime[] = expireTime.split(" : ");
            Log.e("hour", splitTime[0]);

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

            String currentTime = sdf.format(c.getTime());

            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

            Date d1 = null;
            Date d2 = null;
            try {
                d1 = format.parse(currentTime);
                d2 = format.parse(noSpaceTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long subractedTime = d2.getTime() - d1.getTime();
            diffSeconds = subractedTime / 1000;
            diffMinutes = subractedTime / (60 * 1000);
            diffHours = subractedTime/ (60 * 60 * 1000);

            System.out.println("Time in seconds: " + diffSeconds + " seconds.");
            System.out.println("Time in minutes: " + diffMinutes + " minutes.");
            System.out.println("Time in hours: " + diffHours + " hours.");



            if (cn.getRemind_me().contains("3 days before expiry") && numdays == 3){
                Log.d("ExDate: ", "hello");



               AlarmManager alarmManager1 = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

                Intent notificationIntent1 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                notificationIntent1.addCategory("android.intent.category.DEFAULT");

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.HOUR, (int)diffHours);
                cal.add(Calendar.MINUTE, (int)diffMinutes);
                cal.add(Calendar.SECOND, (int)diffHours);

                notificationIntent1.putExtra("three", "three");
                notificationIntent1.putExtra("item3", cn.getItem_name());

                PendingIntent broadcast1 = PendingIntent.getBroadcast(getActivity(), 100, notificationIntent1, PendingIntent.FLAG_UPDATE_CURRENT);

                alarmManager1.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast1);


            }
            else if (cn.getRemind_me().contains("7 days before expiry") && numdays == 7){
                Log.d("ExDate: ", "hello2");



            }

            else if (cn.getRemind_me().contains("1 day before expiry") && numdays == 1){
                Log.d("ExDate: ", "hello3");



            }
            Log.d("Name: ", log);
        }









       /* int numberOfColumns = 2;

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));*/
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(getActivity(), data);
        recyclerView.setAdapter(myRecyclerViewAdapter);

        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorPrimaryDark));


        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {

            @Override
            public boolean onQueryTextChange(String query) {
                final List<itemModel> filteredModelList = filter(data, query);

                myRecyclerViewAdapter.setFilter(filteredModelList);

                return true;

            }
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

        });


        super.onCreateOptionsMenu(menu, inflater);


    }
    private List<itemModel> filter(List<itemModel> models, String query) {
        query = query.toLowerCase();
        final List<itemModel> filteredModelList = new ArrayList<>();
        for (itemModel model : models) {
            final String text = model.item_name.toLowerCase();
            final String cname = model.item_name.toLowerCase();
            if (text.contains(query) || cname.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.action_search:   //this item has your app icon
                return true;
            case android.R.id.home:

                getActivity().finish();
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }



}

